const isHTML = (str) => {
	const a = document.createElement("div");
	a.innerHTML = str;

	for (let c = a.childNodes, i = c.length; i--;) {
		if (c[i].nodeType === 1) return true;
	}

	return false;
};

const innerText = (htmlString) => {
	return htmlString.replace(/<[^>]*>/g, "").trim();
};

const foundContainerELement = (rootElements, containerElement) => {
	return rootElements.find((element) => {
		return element === containerElement;
	});
};

export {
	isHTML,
	innerText,
	foundContainerELement
};